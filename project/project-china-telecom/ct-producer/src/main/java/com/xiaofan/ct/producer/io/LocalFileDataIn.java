package com.xiaofan.ct.producer.io;

import com.xiaofan.ct.common.bean.Data;
import com.xiaofan.ct.common.bean.DataIn;
import com.xiaofan.ct.producer.bean.Contact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 本地文件输入
 */
public class LocalFileDataIn implements DataIn {

    private BufferedReader reader = null;

    public LocalFileDataIn(String path) {
        setPath(path);
    }

    @Override
    public void setPath(String path) {
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object read() throws IOException {
        return null;
    }

    @Override
    public <T extends Data> List<T> read(Class<T> clazz) throws IOException {

        List<T> ts = new ArrayList<>();

        try {
            // 从数据文件中读取所有的数据
            String line = null;

            while ((line = reader.readLine()) != null) {
                // 将数据转换为指定类型的对象，封装为集合返回
                T t = clazz.newInstance();
                t.setValue(line);

                ts.add(t);
            }
        } catch (Exception e) {

        }

        return ts;
    }

    @Override
    public void close() throws IOException {
        if (reader != null) reader.close();
    }
}
