package com.xiaofan.ct.common.bean;

import java.io.Closeable;
import java.io.IOException;

public interface DataOut extends Closeable {
    void setPath(String path);

    void write(Object data) throws IOException;

    void write(String data) throws IOException;
}
