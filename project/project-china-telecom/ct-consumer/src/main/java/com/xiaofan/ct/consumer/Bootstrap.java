package com.xiaofan.ct.consumer;

import com.xiaofan.ct.common.bean.Consumer;
import com.xiaofan.ct.consumer.bean.CallLogConsumer;

import java.io.IOException;

/**
 * 启动消费者
 * 使用kafka消费者获取Flume采集的数据
 * 将数据存储到hbase中
 */
public class Bootstrap {
    public static void main(String[] args) throws IOException {
        // 创建消费者
        Consumer consumer = new CallLogConsumer();
        consumer.consume();
        consumer.close();
    }
}
