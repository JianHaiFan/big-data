package com.xiaofan.scala

import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment, _}
import org.apache.flink.streaming.api.windowing.time.Time


/**
 * 流处理 WordCount
 */
object SocketWindowWordCount_B0002 {
  def main(args: Array[String]): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val data: DataStream[String] = env.socketTextStream("192.168.1.27", 9999)

    val result: DataStream[(String, Int)] = data
      .flatMap(_.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .keyBy(_._1)
      .sum(1)

    result.print()

    env.execute("SocketWindowWordCount_B0002")

  }
}
