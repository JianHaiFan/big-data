package com.xiaofan.scala

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment, _}


/**
 * 批处理 WordCount
 */
object WorldCount_B0001 {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val inputPath = "D:\\big-data\\FlinkDemo\\src\\main\\resources\\hello.txt"

    val data: DataSet[String] = env.readTextFile(inputPath)

    val result: AggregateDataSet[(String, Int)] = data
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(0)
      .sum(1)

    result.print()


  }
}
