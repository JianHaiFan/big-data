package com.xiaofan.orientobject

// 伴生对象：与类名相同并且在同一个文件中
object Student {
  def main(args: Array[String]): Unit = {
    val s = new Student("xiaofan",20)
    println(s.name +" " +  s.age + " " + s.showFaceValue + " " + s.height)
    println("----------------------------------------------------------")
    val ss = new Student("xiaofan", 20,"male")
    println(ss.name +" " +  ss.age + " " + ss.showFaceValue + " " + ss.height + " " + ss.showGender)
  }
}

/**
 * 每个类都有主构造器，主构造器的参数直接放置类名后面，与类交织在一起
 * 对于没有val修饰也没有var修饰的构造器参数，访问的权限，只限于类内部，在伴生对象内部也直接访问不到， 这里和在定义成员变量this的功能相似
 * 在主构造器的类名后面加private修饰后，实例化只能在伴生对象里面实例化
 */
class Student(val name: String, var age: Int, faceValue: Double = 99.99, private var height: Int = 170){

  private [this] var gender: String = null

  //主构造器会执行类定义中的所有语句
  println("执行主构造器gender: " + gender)

  // 辅助构造器 def this (参数)
  def this(name: String, age: Int, gender: String){
    //每个辅助构造器必须以主构造器或其他的辅助构造器的调用开始
    this(name, age)
    println("执行辅助构造器")
    this.gender = gender
  }

  // 自定义方法
  def showFaceValue: Double = faceValue

  def showGender: String = this.gender
}
