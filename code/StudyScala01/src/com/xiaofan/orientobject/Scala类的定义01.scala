package com.xiaofan.orientobject

object Scala之面向对象01 {
  def main(args: Array[String]): Unit = {
    // 在Scala中，类并不用声明为public。
    // Scala源文件中可以包含多个类，所有这些类都具有公有可见性。
  }
}

class Person {
  // 用val修饰的变量是只读属性，有getter但没有setter（相当与Java中用final修饰的变量）
  val id = "9527"

  // 用var修饰的变量既有getter又有setter
  var age: Int = 18

  // 私有字段,只能在类的内部或伴生对象中使用  伴生对象
  private var name: String = "唐伯虎"

  // 私有字段,访问权限更加严格的，只有Person类的方法能访问到当前对象的字段
  private[this] val pet = "小强"

}
