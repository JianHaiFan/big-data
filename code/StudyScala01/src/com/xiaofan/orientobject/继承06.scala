package com.xiaofan.orientobject

object 继承06 {
  // 扩展类
  // 在Scala中扩展类的方式和Java一样都是使用extends关键字
  // 重写方法
  // 在Scala中重写一个非抽象的方法必须使用override修饰符
  // 类型检查和转换
  // Scala						Java
  //  obj.isInstanceOf[C]			obj instanceof C
  //  obj.asInstanceOf[C]			(C)obj
  //  classOf[C]					C.class

  // 注意： Type与Class、ClassTag与TypeTag的细节对比：https://blog.csdn.net/zero__007/article/details/51757436
  // 超类的构造

  /**
   * trait类似于Java8中接口的功能，接口中可以有实现的方法
   */
  trait Flyable {
    def fly(): Unit ={
      println("I can fly...")
    }

    def fight(): String
  }

  abstract class Animal {
    def run(): Int
    val name: String
  }
  /**
   * 如果没有抽象类，之后多个接口的时候，第一个依然要用extends关键字,后续的用with
   */
  class Human extends Animal with Flyable {

    override val name: String = "abc"

    override def run(): Int = 1
    //在子类中重写超类的抽象方法时，不需要使用override关键字，写了也可以
    override def fight(): String = {
      "aaa"
    }
    //在Scala中重写一个非抽象方法必须用override修饰
    override def fly(): Unit = {
      println("我可以自己飞...")
    }
  }
}
