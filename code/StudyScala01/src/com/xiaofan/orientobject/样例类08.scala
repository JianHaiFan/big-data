package com.xiaofan.orientobject

object 样例类08 {

}

// 在Scala中样例类是一中特殊的类，可用于模式匹配。case class是多例的，后面要跟构造参数，case object是单例的

import scala.util.Random

case class SubmitTask(id: String, name: String)

case class HeartBeat(time: Long)

case object CheckTimeOutTask

object CaseDemo6 extends App {
  val arr = Array(CheckTimeOutTask, HeartBeat(123), HeartBeat(88888),HeartBeat(8), SubmitTask("0001", "task-0001"))
  val a = arr(Random.nextInt(arr.length))
  println(a)
  a match {
    case SubmitTask(id, name) => {
      println(s"$id, $name")
    }
    case HeartBeat(time) => {
      println(time)
    }
    case CheckTimeOutTask => {
      println("check...")
    }
  }
}

//注意：
//1. case class 可以new，也可以不用new
//2. case class 不仅可以模式匹配，还可以传递数据
//3. case object 也可以进行模式匹配，不需要封装数据
//4. 样例类多用于actor编程模型，后续actor逐步由AKKA取代
