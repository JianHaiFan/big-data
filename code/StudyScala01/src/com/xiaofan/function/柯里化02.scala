package com.xiaofan.function

object 柯里化02 {
  def main(args: Array[String]): Unit = {
    // 里化指的是将原来接受两个参数的方法变成新的接受一个参数的方法的过程
  }
}

object Context {
  implicit val a: Int = 1000
}

object FunDemo {
  def main(args: Array[String]) {
    // 会到相应的类下面找与对应隐士值类型一样的值
    import Context._
    f(2)
  }

  def f(x: Int)(implicit y: Int = 100): Unit = {
    // 结果为2000
    println(x * y)
  }
}
