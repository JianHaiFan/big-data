package com.xiaofan.basic

object 变量的声明01 {
  def main(args: Array[String]): Unit = {
    // 使用val定义的变量值是不可以改变的，相当于Java里用final修饰的变量
    val i = 1
    // 使用var定义的变量是可变的，在Scala中鼓励使用val
    var s = "hello"
    // scala编译器会自动推断变量的类型，必要的时候可以指定类型，变量名在前，类型在后
    val str: String = "hello scala!"

    println("i = " + i + ",s = " + s + ", str = " + str)
  }
}
