package com.xiaofan.basic

object Scala之元组12 {
  def main(args: Array[String]): Unit = {
    // 在Scala中，元组类似Python中的元组
    val t = (1.0, 2, "spark", 'c')
    t._1 // 获取第一个元素1.0
    t._2 // 获取第二个元素2
    // 注意： a. Scala中，元组下标从1开始，Python中是从0开始
    // b. Map中实际上存放的就是一个特殊的元组（对偶元组）

    println(t._1)
    println(t._2)
  }
}
