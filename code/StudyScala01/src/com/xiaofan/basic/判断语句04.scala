package com.xiaofan.basic

object 判断语句04 {
  def main(args: Array[String]): Unit = {
    // 判断x的值，将结果赋予y
    val x = 1
    val y = if (x > 0) 1 else -1
    println(y)

    // 混合表达式,将结果赋予z
    val z = if (x > 1) 1 else "error"
    println(z)

    // 缺失else，相当于 if(x > 2) 1 else ()
    // 在scala中每个表达式都有值，scala中有个Unit类，写做(), 相当于Java中的void
    val m = if (x > 2) 200
    println(m)

    // if和else if
    val k = if (x > 0) 0 else if (x >= 1) 1 else 2
    println(k)

    // 块表达式
    // 在scala中{}中可包含一系列表达式，块中最后一个表达式的值就是块的值
    val result = {
      if (x < 0) {
        -1
      } else if (x >= 1) {
        1
      } else {
        "error"
      }
      val a = "a"
      100
    }
    println(result)


  }
}
