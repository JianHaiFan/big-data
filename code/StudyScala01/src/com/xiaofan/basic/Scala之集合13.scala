package com.xiaofan.basic

import scala.collection.mutable.{HashSet, ListBuffer, Set}

object Scala之集合13 {
  def main(args: Array[String]): Unit = {
    // Scala的集合有三大类：序列Seq、集Set、映射Map，所有的集合都扩展自Iterable特质，在Scala中集合有可变（mutable）和不可变（immutable）两种类型
    // 集合
    // 在Scala中列表要么为空（Nil标表示空列表）要么是一个head元素加上一个tail列表
    // 9::List(5,2)	:: 操作符是将给定的头和尾创建一个新列表，右结合
    // 不可变集合
    val lst = List(1, 2, 3, 4)

    // 将0插入到lst的前面生成一个新的List
    val lst1 = 0 :: lst
    val lst2 = lst.::(0)
    val lst3 = 0 +: lst
    val lst4 = lst.+:(0)

    val lst5 = lst :+ 3 // 将一个元素添加到lst的后面差生一个新的集合
    val lst6 = lst ++ List(7, 8, 9) // 将两个List合并成一个新的List
    val lst7 = lst ++: List(7, 8, 9)
    val lst8 = lst.:::(List(7, 8, 9)) // 将后面的集合插入到lst的前面，生成一个新的集合

    // 可变集合
    val lb = ListBuffer(1, 2, 3)

    lb += 7 // lb中追加一个元素
    lb -= 7 // lb中去掉相应的元素
    lb += (8, 9, 10) // lb中追加多个元素
    lb -= (8, 9, 10) // lb中去掉相应的多个元素
    val newlst1 = lb ++ List(-1, -2) // 将后面的集合与lb合并，并且生成新的集合
    val newlst2 = lb -- List(-1, -2) // 将后面的集合从lb中剔除，生成新的集合，但是不改变lb集合


    // Set
    // 不可变的Set
    val set1 = new HashSet[Int]()
    val set2 = set1 + 4 // 将一个元素和set1合并生成一个新的set2，原有的set不变
    val set3 = set1 - 4 // 将一个元素和set1合并生成一个新的set2，原有的set不变
    val set4 = set1 ++ Set(5, 6, 7)
    val set5 = set1 -- Set(5, 6, 7) // 将相应的多个元素从set1集合中剔除，生成新的set，原有的set不变
    // 可变的Set
    // val set1 = new HashSet[Int]()

    set1 += 2 // 向HashSet中添加一个元素 等价于add(2)
    set1 -= 2 // 删除元素 等价remove(2)
    set1 ++= Set(3, 4, 5) // 向HashSet中添加多个元素
    set1 --= Set(3, 4, 5) // 从HashSet剔除多个相应的元素

  }
}
