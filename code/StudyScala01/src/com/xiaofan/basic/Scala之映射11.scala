package com.xiaofan.basic

import scala.collection.mutable.Map

object Scala之映射11 {
  def main(args: Array[String]): Unit = {
    // 在Scala中，把哈希表这种数据结构叫做映射，Java中叫Map，Python中叫字典
    // 可变映射
    // import scala.collection.mutable._
    // val m = Map("a" -> 1,"b" -> 2, "c" -> 3)
    // 不可变映射
    // import scala.collection.immutable._
    // val m = Map("a" -> 1,"b" -> 2, "c" -> 3)
    // 通过元组构建映射
    val m = Map(("a", 1), ("b", 2), ("c", 3))
    // 向可变映射中追加（映射无序）
    m += (("d", 4), ("e", 5))
    m += ("f" -> 6, "g" -> 7)

    // 常用方法
    m.getOrElse("a", -1) // 底层通过模式匹配实现  不会抛异常


  }
}
