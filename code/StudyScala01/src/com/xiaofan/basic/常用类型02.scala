package com.xiaofan.basic

object 常用类型02 {
  def main(args: Array[String]): Unit = {
    // Scala和Java一样， 有7种数值类型Byte、Char、Short、Int、Long、Float、Double（无包装类型）和一个Boolean类型。
    println("Scala和Java一样， 有7种数值类型Byte、Char、Short、Int、Long、Float、Double（无包装类型）和一个Boolean类型。")
  }
}
