package com.xiaofan.basic

object Scala操作符重载05 {
  def main(args: Array[String]): Unit = {
    // Scala中的+-*/%等操作符的作用和Java一样，位于&|^>><<也一样。只是一点特别的：这些操作符实际上是方法
    // 例如：
    // a+b ==> a.+(b)
    val a = 100
    val b = 200
    val result1 = a + b
    val result2 = a.+(b)
    println(result1)
    println(result2)
  }
}
