package com.xiaofan.basic

object 方法与函数的转换08 {
  def main(args: Array[String]): Unit = {
    // 函数作为参数传入方法
    val arr = Array[Int](1, 3, 5, 7, 9)
    // 匿名函数形式
    val result = arr.map((x: Int) => x * 100)
    val result1 = arr.map((x) => x * 100)
    val result2 = arr.map(x => x * 100)
    val result3 = arr.map(_ * 100)
    for (e <- result) println(e)

    // 将函数传入自定义方法
    def m(f: Int => Int): Unit = {
      println(f(100))
    }

    // 简版
    val f0 = (x: Int) => x * x
    m(f0)
    // 完整版本
    val f1: Int => Int = _ * 2
    m(f1)


    /**
     * 方法转换成函数，方法作为参数传入方法
     */
    def method(f: Int => Int) {
      println(f(100))
    }

    def m1(x: Int) = x * x

    method(m1 _)

    // 注意： 将方法名传入方法的本质是将方法转换成了函数
    method(m1)

    // 函数和方法的总结
    /**
     * a. 在Java中方法和函数是一样的，在Scala中方法和函数是有区别的，不能混为一体
     * b. 函数和方法具有的功能是不一样的
     * c. 函数的标识 => ,但有=> 不一定是函数
     * d. 函数是可以作为参数传入到方法里面的，方法也可以作为参数传入到方法里面，其本质还是转换成了函数
     */


  }
}
