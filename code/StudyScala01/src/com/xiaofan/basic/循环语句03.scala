package com.xiaofan.basic

object 循环语句03 {
  def main(args: Array[String]): Unit = {
    // for循环1
    val arr = Array[Int](1, 2, 3)
    for (e <- arr) println(e)

    // for循环2
    for (e <- 1 to 3) println(e)

    // 高级for循环
    // 每个生成器都可以带一个条件，注意： if前面没有分号
    for (i <- 1 to 3; j <- 1 to 3 if i != j)
      print(i * 10 + j + " ")
    println()

    // for循环推导式1
    // 如果for循环的循环体以yield开始，则该循环会构建出一个集合，每次迭代生成集合中的一个值
    val v = for (i <- 1 to 10) yield i * 10
    println(v)

    // for循环推导式2
    // 遍历数组通过索引获取元素时，使用小括号括起来
    val arr1 = Array[String]("a", "b", "c", "d")
    for (i <- 0 until arr1.length) println(arr1(i))
  }
}
