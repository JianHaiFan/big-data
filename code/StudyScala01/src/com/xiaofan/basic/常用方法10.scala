package com.xiaofan.basic

object 常用方法10 {
  def main(args: Array[String]): Unit = {
    val arr = Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    // 常用方法
    // val a1 = arr.map(_ * 10)	// 每一项乘以 10
    // val a2 = arr.filter(_ % 2 != 0)	// 把满足条件的元素过滤出来（过滤奇数）
    // 排序
    // 升序
    // val a3 = arr.sorted		// 这里涉及到了隐式转换
    // val a3_1 = arr.sortWith(_<_)
    // 降序
    // val a4 = arr.sorted.reverse
    // val a4_1 = arr.sortWith(_>_)
    // 按照某种方式排序
    //  arr.sortBy(x => x)	// 按照原有数据类型（Int）进行排序
    // arr.sortBy(x => x + “”)	// 按照String进行排序
    // 求和
    // arr.sum
    // 聚合 reduce --> 非并行集合会调用reduceLeft ???
    // arr.reduce(_+_)
    // 叠加 fold --> 非并行集合会调用foldLeft， 它与reduce的区别是可以给一个初始值
    // arr.fold(10)(_+_)
    // 叠加求和
    // val arr = List(List(1,2,3,4),List(5,6,7,8),List(9,10),List(11))
    // arr.aggregate(0)((a,b)=>a+b.sum,(x,y) => x+y)
    // arr.aggregate(0)(_+_.sum,_+_)	// 这里是一个逗号
  }
}
