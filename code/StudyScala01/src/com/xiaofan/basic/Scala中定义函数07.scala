package com.xiaofan.basic

object Scala中定义函数07 {
  def main(args: Array[String]): Unit = {
    // 定义有名字的函数
    // 没有指定返回类型（简版）
    val f1 = (x: Int) => x * 10
    val f2 = (x: Int, y: Int) => x + y

    // 指定了返回类型（完整版）
    // 注意：中间的Int是返回值
    val f3: Int => Int = { x => x * 100 }
    val f4: Int => String = { i => i.toString }
    // 注意：在Scala中，元组的下标是从1开始，python中是从0开始,r._1 获取到的是3.0
    val f5: (Double, Int) => (Int, Double) = { (x, y) => (y, x) }

    // 定义匿名函数
    // (x: Int) => x * 10
    // (x: Int,y: Int) => x * y + 10
  }
}
