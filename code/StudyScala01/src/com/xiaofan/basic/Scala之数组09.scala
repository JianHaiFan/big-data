package com.xiaofan.basic

import scala.collection.mutable.ArrayBuffer

object Scala之数组09 {
  def main(args: Array[String]): Unit = {
    // 在Scala中，数组分为定长数组和变长数组
    // 定长数组
    val arr1 = new Array[Int](8) // 初始化一个长度为8的定长数组，所有元素均为0
    val arr2 = Array[Int](8) // 初始化一个长度为1的定长数组，其值为8
    val arr3 = Array("hadoop", "flink", "spark") // 定义一个长度为3的定长数组
    arr1(0) // 使用()来访问元素
    val arr = Array("hadoop", 1.0, 2) // 定义一个长度为3的混合型的定长数组
    arr(1).asInstanceOf[Double] // 转换Any为具体的Double类型


    // 变长数组
    val ab = new ArrayBuffer[Int]()
    ab += 1 // 尾部追加1个元素
    ab += (2, 3, 4) // 尾部追加多个元素
    ab ++= Array(5, 6) // 尾部追加一个数组，ab里面是铺平的
    ab ++= ArrayBuffer(3, 5)
    ab --= Array(5, 6) // 从ab下标为0的位置开始找到相应的元素，删掉
    ab --= ArrayBuffer(3, 5)
    ab.insert(0, -1, 0) // 在下标为0的位置处，插入两个元素
    ab.remove(1, 2) // 删除下标为1开始的2个元素


    // 遍历数组
    // 增强for循环
    for (e <- arr) print(e + " ")
    // 通过下标循环
    for (i <- 0 until arr.length) print(arr(i) + " ")
    // 逆序输出
    for (i <- (0 until arr.length).reverse) print(arr(i) + " ")

    // 数组转换
    // yield 关键字将原始的数组进行转换会产生一个新的数组，原始的数组保持不变
    val res1 = for (e <- ab) yield e * 10
    val a1 = ab.map(_ * 10) // 每一项乘以 10  （升级版）
    // 偶数项乘以10
    val res2 = for (e <- ab if e % 2 == 0) yield e * 10
    val res3 = ab.filter(_ % 2 == 0).map(_ * 10) //（升级版）

  }
}
