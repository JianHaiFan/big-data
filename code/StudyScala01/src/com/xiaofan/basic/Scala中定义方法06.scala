package com.xiaofan.basic

object Scala中定义方法06 {
  def main(args: Array[String]): Unit = {
    // 定义既有参数，又有返回值方法
    // 注意： 函数的返回类型，可以不写，但是在递归方法中必须指明返回类型。
    def m1(x: Int): Int = x * x

    def m2(x: Int) = x * x

    // 定义没有返回值的方法
    def m3(x: Int, y: Int) = println(x + y)

    // 如果没有返回值，也可以不用写=
    def m4(x: Int, y: Int) {
      println(x + y)
    }

    // main方法的定义
    def main(args: Array[String]): Unit = {
      println(args.length)
    }
  }
}
