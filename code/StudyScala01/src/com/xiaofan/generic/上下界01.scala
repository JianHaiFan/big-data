package com.xiaofan.generic

class GranderFather

class Father extends GranderFather

class Son extends Father

class Student

object Card{

  def getIDCard[T >: Son](person:T): Unit ={
    println("OK,交给你了")
  }
  def main(args: Array[String]): Unit = {
    getIDCard[GranderFather](new Father)
    getIDCard[GranderFather](new GranderFather)
    getIDCard[GranderFather](new Son)
    // getIDCard[GranderFather](new Student) //报错，所以注释
  }
}
