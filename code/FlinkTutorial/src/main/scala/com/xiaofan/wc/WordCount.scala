package com.xiaofan.wc

import org.apache.flink.api.scala._

/**
 * 批处理 WordCount
 */
object WordCount {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val inputPath: String = "D:\\big-data\\code\\FlinkTutorial\\src\\main\\resources\\hello.txt";
    val data: DataSet[String] = env.readTextFile(inputPath)

    val result: AggregateDataSet[(String, Int)] = data
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(0)
      .sum(1)

    result.print()

  }
}
