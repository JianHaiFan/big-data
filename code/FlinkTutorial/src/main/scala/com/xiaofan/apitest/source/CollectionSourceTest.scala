package com.xiaofan.apitest.source

import org.apache.flink.streaming.api.scala._

/**
 * 传感器样例类
 */
case class SensorReading(id: String, timestamp: Long, temperature: Double)

object CollectionSourceTest {
  def main(args: Array[String]): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val dataList: List[SensorReading] = List(
      SensorReading("sensor_1", 1547718199, 35.8),
      SensorReading("sensor_6", 1547718201, 15.4),
      SensorReading("sensor_7", 1547718202, 6.7),
      SensorReading("sensor_10", 1547718205, 38.1))

    val value: DataStream[SensorReading] = env.fromCollection(dataList)
    value.print()

    env.execute("collection source test")

  }
}




















