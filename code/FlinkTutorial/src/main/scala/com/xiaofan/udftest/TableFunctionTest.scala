package com.xiaofan.udftest

import com.xiaofan.apitest.source.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{Table, _}
import org.apache.flink.table.functions.TableFunction
import org.apache.flink.types.Row

object TableFunctionTest {
  def main(args: Array[String]): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val tabEnv: StreamTableEnvironment = StreamTableEnvironment.create(env)

    val inputPath = "D:\\big-data\\code\\FlinkTutorial\\src\\main\\resources\\sensor.txt"
    val inputStream: DataStream[String] = env.readTextFile(inputPath)

    val dataStream: DataStream[SensorReading] = inputStream.map(
      data => {
        val arr: Array[String] = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      }
    ).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.milliseconds(3)) {
      override def extractTimestamp(element: SensorReading) = element.timestamp * 1000L
    })

    val sensorTable: Table = tabEnv.fromDataStream(dataStream, $"id", $"temperature", $"timestamp".rowtime() as "ts")

    // 调用自定义函数，对id进行hash运算
    // 1. table api
    val split = new Split("_")
    val resultTable: Table = sensorTable
      .joinLateral(split($"id") as("word", "length"))
      .select($"id", $"temperature", $"word", $"length")

    // 2. sql
    tabEnv.createTemporaryView("sensor", sensorTable)
    tabEnv.createTemporaryFunction("split", split)

    val sqlResultTable: Table = tabEnv.sqlQuery(
      """
        |select
        | id, ts, word, length
        |from
        | sensor, lateral table( split(id) ) as splitid (word, length)
        |""".stripMargin)


    tabEnv.toAppendStream[Row](sqlResultTable).print("sql ")

    env.execute("function test")


  }
}


/**
 * 自定义Table函数
 */

class Split(var separator: String) extends TableFunction[(String, Int)] {

  def eval(str: String): Unit = {
    str.split(separator).foreach(
      word => collect((word, word.length))
    )
  }
}

