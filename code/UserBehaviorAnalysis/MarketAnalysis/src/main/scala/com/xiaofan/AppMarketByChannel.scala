package com.xiaofan

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

import java.sql.Timestamp
import java.util.UUID
import scala.util.Random

/**
 * 数据输入样例类
 */
case class MarketUserBehavior(userId: String, behavior: String, channel: String, timestamp: Long)

/**
 * 定义数据样例类
 */
case class MarketViewCount(windowStart: String, windowEnd: String, channel: String, behavior: String, count: Long)

/**
 * 自定义测试数据
 */
class SimulatedSource() extends RichSourceFunction[MarketUserBehavior] {

  var isRunning = true

  // 定义用户行为和渠道集合
  val behaviors: Seq[String] = Seq("view", "download", "install", "uninstall")

  val channels: Seq[String] = Seq("appstore", "weibo", "weichat", "tieba")

  val rand: Random = Random

  override def run(ctx: SourceFunction.SourceContext[MarketUserBehavior]): Unit = {
    val maxCounts = Long.MaxValue
    var count = 0L

    // while 循环，不停地随机产生数据
    while (isRunning && count < maxCounts) {
      val id = UUID.randomUUID().toString
      val behavior = behaviors(rand.nextInt(behaviors.size))
      val channel = channels(rand.nextInt(channels.size))
      val ts = System.currentTimeMillis()

      ctx.collect(MarketUserBehavior(id, behavior, channel, ts))

      count += 1

      Thread.sleep(50L)
    }
  }

  override def cancel(): Unit = isRunning = false
}

object AppMarketByChannel {
  def main(args: Array[String]): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val dataStream: DataStream[MarketUserBehavior] = env.addSource(new SimulatedSource)
      .assignAscendingTimestamps(_.timestamp)

    val resultStream: DataStream[MarketViewCount] = dataStream
      .filter(_.behavior != "uninstall")
      .keyBy(data => (data.channel, data.behavior))
      .timeWindow(Time.days(1), Time.seconds(5))
      .process(new MarketCountByChannel())

    resultStream.print()

    env.execute("market by channel test")
  }
}


/**
 * 自定义ProcessWindowFunction
 */
class MarketCountByChannel extends ProcessWindowFunction[MarketUserBehavior, MarketViewCount, (String,String), TimeWindow] {
  override def process(key: (String, String), context: Context, elements: Iterable[MarketUserBehavior], out: Collector[MarketViewCount]): Unit = {
    val start = new Timestamp(context.window.getStart).toString
    val end = new Timestamp(context.window.getEnd).toString
    val channel = key._1
    val behavior = key._2
    val count = elements.size
    out.collect(MarketViewCount(start, end, channel, behavior, count))
  }
}
