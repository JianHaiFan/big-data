package com.xiaofan

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.util.Collector


object TxMatchWithJoin {
  def main(args: Array[String]): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val inputStream1: DataStream[String] = env.readTextFile("D:\\big-data\\code\\UserBehaviorAnalysis\\OrderPayDetect\\src\\main\\resources\\OrderLog.csv")
    val orderEventStream: KeyedStream[OrderEvent, String] = inputStream1
      .map {
        data => {
          val arr: Array[String] = data.split(",")
          OrderEvent(arr(0).toLong, arr(1), arr(2), arr(3).toLong)
        }
      }
      .assignAscendingTimestamps(_.timestamp * 1000L)
      .filter(_.eventType == "pay")
      .keyBy(_.txId)

    val inputStream2: DataStream[String] = env.readTextFile("D:\\big-data\\code\\UserBehaviorAnalysis\\OrderPayDetect\\src\\main\\resources\\ReceiptLog.csv")
    val receiptEventStream: KeyedStream[ReceiptEvent, String] = inputStream2
      .map {
        data => {
          val arr: Array[String] = data.split(",")
          ReceiptEvent(arr(0), arr(1), arr(2).toLong)
        }
      }
      .assignAscendingTimestamps(_.timestamp * 1000L)
      .keyBy(_.txId)

      // 3. join两条流
    val resultStream: DataStream[(OrderEvent, ReceiptEvent)] = orderEventStream.intervalJoin(receiptEventStream)
      .between(Time.seconds(-3), Time.seconds(5))
      .process(new TxMatchWithJoinResult())

    resultStream.print()


    env.execute("tx job test")

  }

}

class TxMatchWithJoinResult() extends ProcessJoinFunction[OrderEvent, ReceiptEvent, (OrderEvent, ReceiptEvent)] {
  override def processElement(left: OrderEvent, right: ReceiptEvent, ctx: ProcessJoinFunction[OrderEvent, ReceiptEvent, (OrderEvent, ReceiptEvent)]#Context, out: Collector[(OrderEvent, ReceiptEvent)]): Unit = {
    out.collect((left, right))
  }
}





