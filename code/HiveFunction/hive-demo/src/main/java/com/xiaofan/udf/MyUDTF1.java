package com.xiaofan.udf;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 输入
 * hello,flink:hello,spark
 * a,A:b,B
 * <p>
 * 输出：
 * hello flink
 * hello spark
 */
public class MyUDTF1 extends GenericUDTF {

    private ArrayList<String> outList = new ArrayList<>();

    @Override
    public StructObjectInspector initialize(StructObjectInspector argOIs) throws UDFArgumentException {
        //1.定义输出数据的列名和类型
        List<String> fieldNames = new ArrayList<>();
        List<ObjectInspector> fieldOIs = new ArrayList<>();

        //2.添加输出数据的列名和类型
        fieldNames.add("key1");
        fieldNames.add("value1");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);

        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);

    }

    // 处理输入数据
    @Override
    public void process(Object[] args) throws HiveException {
        //1.获取原始数据
        String arg = args[0].toString();

        //3.将原始数据按照传入的分隔符进行切分
        String[] items = arg.split(":");

        //4.遍历切分后的结果，并写出
        for (String item : items) {

            //集合为复用的，首先清空集合
            outList.clear();

            String[] fields = item.split(",");

            for (String field: fields) {
                //将每一个单词添加至集合
                outList.add(field);
            }

            //将集合内容写出
            forward(outList);
        }

    }

    @Override
    public void close() throws HiveException {

    }
}
